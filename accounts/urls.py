from django.contrib import admin
from django.urls import path
from . import views

app_name = 'accounts'

urlpatterns = [
	path('', views.pagepertama, name='pagepertama'),
    path('login/', views.pagelogin, name='pagelogin'),
    path('menu/', views.pagemenu, name='pagemenu'),
    path('logout/',views.logoutpage, name='logoutpage'),
]
