from django.test import TestCase, Client
from django.urls import resolve
from django.http import HttpRequest
from accounts.views import pagelogin, pagepertama
from django.contrib.auth.models import User
# Create your tests here.

class UnitTestStory9(TestCase):

	@classmethod
	def setUpTestData(cls):
		cls.u1 = User.objects.create_user(username='testuser', password='testpassword')

	def test_notexist_url_is_notexist(self):
		response = Client().get('/gaada/')
		self.assertEqual(response.status_code, 404)
	
	def test_pagelogin_url_is_exist_use_login_template(self):
		response = Client().get('/login/')
		self.assertEqual(response.status_code, 200)
		self.assertTemplateUsed(response, 'pagelogin.html')

	def test_pagelogin_using_login_function(self):
		response = resolve('/login/')
		self.assertEqual(response.func, pagelogin)

	def test_pagelogin_title_is_right(self):
		request = HttpRequest()
		response = pagelogin(request)
		html_response = response.content.decode('utf8')
		self.assertIn('<title>WOWOWOW</title>', html_response)

	def test_pagepertama_url_is_exist_use_landing_template(self):
		response = Client().get('/')
		self.assertEqual(response.status_code, 200)
		self.assertTemplateUsed(response, 'pagepertama.html')

	def test_pagepertama_using_pagepertama_function(self):
		response = resolve('/')
		self.assertEqual(response.func, pagepertama)

	def test_logoutpage_redirected(self):
		response = Client().get('/logout/')
		self.assertEqual(response.status_code, 302)

	def test_pagemenu_redirected_if_not_logged_in(self):
		response = Client().get('/menu/')
		self.assertEqual(response.status_code, 302)

	def test_pagemenu_when_user_is_authenticated(self):
		self.client.force_login(self.u1)
		response = self.client.get('/menu/')
		self.assertEqual(response.status_code, 200)
		self.assertTemplateUsed(response, 'pagemenu.html')
