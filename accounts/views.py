from django.shortcuts import render, redirect
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth import login, logout
from django.contrib.auth.decorators import login_required

# Create your views here.
def pagelogin(request):
	if request.method == 'POST':
		form = AuthenticationForm(data=request.POST)
		if form.is_valid():
			user = form.get_user()
			login(request, user)
			return redirect('accounts:pagemenu')
	else:
		form = AuthenticationForm()
	return render(request, 'pagelogin.html', {'form':form})

def pagepertama(request):
	return render(request, 'pagepertama.html')

def pagemenu(request):
	if request.user.is_authenticated:
		return render(request,'pagemenu.html')
	else:
		return redirect('accounts:pagelogin')

def logoutpage(request):
	if request.user.is_authenticated:
		if request.method == 'POST':
			logout(request)
	return redirect('accounts:pagepertama')

